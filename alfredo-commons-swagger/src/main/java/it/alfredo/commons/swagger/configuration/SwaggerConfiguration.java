package it.alfredo.commons.swagger.configuration;


import com.fasterxml.classmate.TypeResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


import io.swagger.annotations.Api;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

/**
 * <p>
 * Description: TODO Insert description here</p>
 *
 * @author alfredo
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration extends WebMvcConfigurerAdapter {

//    @Autowired
//    private TypeResolver typeResolver;

//    @Override
//    public void addResourceHandlers( ResourceHandlerRegistry registry ) {
//        registry.addResourceHandler( "swagger-ui.html" ).addResourceLocations( "classpath:/META-INF/resources/" );
//        registry.addResourceHandler( "/webjars/**" ).addResourceLocations( "classpath:/META-INF/resources/webjars/" );
//    }

    @Override
    public void addViewControllers( ViewControllerRegistry registry ) {
        registry.addRedirectViewController( "/", "/swagger-ui.html" );
    }

    @Bean
    public Docket petApi() {
        return new Docket( DocumentationType.SWAGGER_2 )
                .select()
                .apis( RequestHandlerSelectors.withClassAnnotation( Api.class ) )
                .paths( PathSelectors.any() )
                .build()
                .pathMapping( "/" ) //                .directModelSubstitute( LocalDate.class,
                //                        String.class )
                //                .genericModelSubstitutes( ResponseEntity.class )
                //                .alternateTypeRules(
                //                        newRule( typeResolver.resolve( DeferredResult.class,
                //                                typeResolver.resolve( ResponseEntity.class, WildcardType.class ) ),
                //                                typeResolver.resolve( WildcardType.class ) ) )
                //                .useDefaultResponseMessages( false )
                //                .globalResponseMessage( RequestMethod.GET,
                //                        newArrayList( new ResponseMessageBuilder()
                //                                .code( 500 )
                //                                .message( "500 message" )
                //                                .responseModel( new ModelRef( "Error" ) )
                //                                .build() ) )
                //                .securitySchemes( newArrayList( apiKey() ) )
                //                .securityContexts( newArrayList( securityContext() ) )
                //                .enableUrlTemplating( true )
                //                .globalOperationParameters(
                //                        newArrayList( new ParameterBuilder()
                //                                .name( "someGlobalParameter" )
                //                                .description( "Description of someGlobalParameter" )
                //                                .modelRef( new ModelRef( "string" ) )
                //                                .parameterType( "query" )
                //                                .required( true )
                //                                .build() ) )
                //                .tags( new Tag( "Pet Service", "All apis relating to pets" ) )
                //                .additionalModels( typeResolver.resolve( AdditionalModel.class ) )
                ;
    }


//    private ApiKey apiKey() {
//        return new ApiKey( "mykey", "api_key", "header" );
//    }
//
//    private SecurityContext securityContext() {
//        return SecurityContext.builder()
//                .securityReferences( defaultAuth() )
//                .forPaths( PathSelectors.regex( "/anyPath.*" ) )
//                .build();
//    }
//
//    List<SecurityReference> defaultAuth() {
//        AuthorizationScope authorizationScope =
//                new AuthorizationScope( "global", "accessEverything" );
//        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
//        authorizationScopes[ 0 ] = authorizationScope;
//        return newArrayList(
//                new SecurityReference( "mykey", authorizationScopes ) );
//    }
//
//    @Bean
//    SecurityConfiguration security() {
//        return new SecurityConfiguration(
//                "test-app-client-id",
//                "test-app-client-secret",
//                "test-app-realm",
//                "test-app",
//                "apiKey",
//                ApiKeyVehicle.HEADER,
//                "api_key",
//                "," /* scope separator */ );
//    }
//
//    @Bean
//    UiConfiguration uiConfig() {
//        return new UiConfiguration(
//                "validatorUrl",// url
//                "none", // docExpansion          => none | list
//                "alpha", // apiSorter             => alpha
//                "schema", // defaultModelRendering => schema
//                UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
//                false, // enableJsonEditor      => true | false
//                true, // showRequestHeaders    => true | false
//                60000L );      // requestTimeout => in milliseconds, defaults to null (uses jquery xh timeout)
//    }
}
