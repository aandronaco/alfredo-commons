 package it.alfredo.commons.util.model; 

import java.io.Serializable;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/** 
*
* @author alfredo 
*/
public class AlfredoObject implements Serializable{

    private static final long serialVersionUID = 1L;

    
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString( this, ToStringStyle.SHORT_PREFIX_STYLE );
    }
    
    @Override
    public boolean equals( final Object o ) {
        return EqualsBuilder.reflectionEquals( this, o );
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode( this );
    }
    
   
}