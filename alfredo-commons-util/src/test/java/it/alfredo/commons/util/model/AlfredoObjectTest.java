/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.alfredo.commons.util.model;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 *
 * @author alfredo
 */
public class AlfredoObjectTest {

    MyAlfredoObject myAlfredoObject1 = new MyAlfredoObject().field1( "myField1" ).field2( "myField2" );
    MyAlfredoObject myAlfredoObject2 = new MyAlfredoObject().field1( "myField1" ).field2( "myField2" );
    MyAlfredoObject myAlfredoObject3 = new MyAlfredoObject().field1( "myFieldWrong" ).field2( "myField2" );


    @Test
    public void testToString() {
        assertThat( myAlfredoObject1.toString(), is( "MyAlfredoObject[field1=myField1,field2=myField2]" ) );

    }

    @Test
    public void testEquals() {

        assertThat( myAlfredoObject1.equals( myAlfredoObject2 ), is( true ) );
        assertThat( myAlfredoObject1.equals( myAlfredoObject3 ), is( false ) );

    }

    @Test
    public void testHashCode() {
        assertThat( myAlfredoObject1.hashCode(), is(myAlfredoObject2.hashCode() ) );
        assertThat( myAlfredoObject1.hashCode(), is(not( myAlfredoObject3.hashCode() )) );

    }


}
