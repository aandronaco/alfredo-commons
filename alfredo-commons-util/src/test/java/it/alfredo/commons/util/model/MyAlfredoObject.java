package it.alfredo.commons.util.model;

/**
 *
 * @author alfredo
 */
public class MyAlfredoObject extends AlfredoObject {

    private String field1;
    private String field2;

    public String getField1() {
        return field1;
    }

    public void setField1( String field1 ) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2( String field2 ) {
        this.field2 = field2;
    }

    public MyAlfredoObject field1( final String value ) {
        this.field1 = value;
        return this;
    }

    public MyAlfredoObject field2( final String value ) {
        this.field2 = value;
        return this;
    }


}
